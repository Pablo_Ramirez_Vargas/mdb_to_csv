'''
creado por Pablo Ramirez Vargas
carpeta f-128
carpeta f-129
carpeta f-158
carpeta f-152
'''
from tkinter import *
from tkinter import messagebox,filedialog
from tkcalendar  import Calendar,DateEntry
from tkinter.ttk import Progressbar
import time
import pyodbc,os,glob
from datetime import *
def button_hover2(v):
    bottondir['bg']='#F5FFFA'
def button_hover_leave2(v):
    bottondir['bg']='#F5F5F5'
def button_hover(v):
    endbutton['bg']='#F5FFFA'
def button_hover_leave(v):
    endbutton['bg']='#F5F5F5'
def select_dir():
    my_file.set(filedialog.askdirectory())#selecionar directorio
    return my_file.get() # return directorio
def main_fun(fecha,directorio,password,progress):
    log=open(directorio+'/'+'log '+ ' '.join((str(datetime.now()).split(':')))+'.txt','x') #crear bitacora
    log.write(str(datetime.now())+'\n')
    log.write('Inicio del proceso'+'\n')
    log.write('Carpeta selecionada '+str(directorio)+'\n')
    log.write(str(fecha[0]+' '+fecha[1])+'\n')
    log.write('Archivo salida ' +directorio+'/' 'Supercep_' +''.join(fecha[0].split('/'))+'_'+''.join(fecha[1].split('/'))+'\n')
    headcheack=False #checar si las columnas estan puestas
    archivos=(glob.glob(directorio+'//**//*.mdb',recursive=True))
    #buscar archivos en directorio de maquina
    print(archivos)
    if len(archivos)>0:
        STEP_OF_PROGRESS=100//len(archivos) #barra de progreso
        stop_button=Button(root,command=lambda:my_stop.set(True),width=7, height=4,bg='#FF6347',border=2,text='Detener')#boton de detener
        stop_button.grid(row=4,column=1,columnspan=2,rowspan=2) #mostrar boton
        fou=open(directorio+'/'+'supercep_' +''.join(fecha[0].split('/'))+'_'+''.join(fecha[1].split('/'))+'.csv', 'x') #crear archivo csv
    else:
        messagebox.showinfo(root,'no se a encontrado archivos')
    count_files=0#contar archivos
    count_lines=0#contar linias
    dict=[]
    for archivo in archivos: #crear loop para leer archivos
        count_files+=1
        MDB = archivo
        DRV='{Microsoft Access Driver (*.mdb, *.accdb)}'#declarar driver de microsoft
        PWD =password #declarar password
        try:
            con = pyodbc.connect('DRIVER={};DBQ={};PWD={}'.format(DRV,MDB,PWD))# connect to db
        except pyodbc.Error as ex :
            messagebox.showinfo(root,message=ex)
            break
        cur = con.cursor() #declarar cursor de sql
        log.write(str(archivo)+'\n')
        if archivo.endswith('.mdb') or archivo.endswith('.MDB') : #buscar .mdb
            for heads in cur.columns('DATOS'):
                if heads.column_name not in dict:
                    dict.append(heads.column_name)
                    fou.write('\"'+heads.column_name+'\",')
            FINALDELARCHIVO=archivo.split('/')
            FINALDELARCHIVO=FINALDELARCHIVO[-1].split('\\')
            cur.close()
            con.close()
        if headcheack==False and len(archivos)-1 == count_files:#BUSCAR SI EL HEADER ES UNICO
            fou.write('\"Formato\",')
            fou.write('\"Maquina\",')
            fou.write('\"Producto\"')
            fou.write('\n')
            headcheack=True
        if my_stop.get()==True:
            messagebox.showinfo(root,message='Proceso detenido')#detener el proceso
            break
    for archivo in archivos:
        count_lines=0
        MDB = archivo
        DRV='{Microsoft Access Driver (*.mdb, *.accdb)}'#declarar driver de microsoft
        PWD =password #declarar password
        try:
            con = pyodbc.connect('DRIVER={};DBQ={};PWD={}'.format(DRV,MDB,PWD))# connect to db
        except pyodbc.Error as ex :
            messagebox.showinfo(root,message=ex)
            break
        cur = con.cursor() #declarar cursor de sql
        FINALDELARCHIVO=archivo.split('/')
        FINALDELARCHIVO=FINALDELARCHIVO[-1].split('\\')
        SQL = 'SELECT * FROM DATOS WHERE Fecha BETWEEN #'+fecha[0]+'# and #'+ fecha[1]+'# ;'#QUERY SQL
        rows = cur.execute(SQL).fetchall()#TODOS LOS RENGLONES DE EL archivo selecionado
        bn=[]
        bn=[c.column_name for c in cur.columns('DATOS')] #columnas del archivo
        for row in rows:
            for enum,e in enumerate(dict): #por cada columna en dict
                if e in bn: #por cada columna en bn
                    fou.write('\"'+str(row[bn.index(e)])+'\",')# por cada campo en row
                else:
                    fou.write('\"'+str('Null')+'\",') # si no se encuetra la columna
            fou.write('\"'+(FINALDELARCHIVO[1]).strip('/')+'\",') #escribir el formato
            fou.write('\"'+(FINALDELARCHIVO[2]).strip('/')+'\",')#escribir el maquina
            fou.write('\"'+'/'.join(FINALDELARCHIVO[-1::])[:-4:]+'\"') #escribir producto
            fou.write('\n')
            count_lines+=1
        log.write('Linias procesadas '+str(count_lines)+'\n')
        progress['value']+=STEP_OF_PROGRESS#barra de progreso
        progress.update()
    if my_stop.get()==False and  len(archivos)>0:
        messagebox.showinfo(root,message='Proceso completado')
    log.write('Proceso cancelado = '+ str(my_stop.get())+'\n')
    cur.close()
    con.close()
root=Tk()
"""todo la interface grafica"""
root.geometry('410x420')
root['bg']='#F5F5F5'
my_stop=BooleanVar(root)
my_stop.set(False)
my_password=StringVar(root)
my_file=StringVar(root)
my_fecha1=StringVar(root)
my_fecha2=StringVar(root)
root.title('Convertir mdbs a csv')
root.resizable(0,0)
bottondir=Button(text='Selecciona un directorio',command=select_dir,bg='#F5F5F5',height=2,width=23 )
bottondir.bind('<Enter>',button_hover2)
bottondir.bind('<Leave>',button_hover_leave2)
label1=Label(root,text='Convertir .mdb a csv ',bg='#F5F5F5')
label2=Label(root,text='Comienzo de datos',bg='#F5F5F5')
label3=Label(root,text='Fin de datos',bg='#F5F5F5')
labelpass=Label(root,text='Password',bg='#F5F5F5')
progress=Progressbar(root, orient = HORIZONTAL,length = 410, mode = 'determinate')
pass_entry=Entry(root,show="*",width=45)
fechaentry1=DateEntry(root)
fechaentry2=DateEntry(root)
endbutton=Button(root,text='Ejecutar',command=lambda:main_fun([fechaentry1.get(),fechaentry2.get()],my_file.get(),pass_entry.get(),progress), width=20, height=4,bg='#F5F5F5')
endbutton.bind('<Enter>', button_hover)
endbutton.bind('<Leave>',button_hover_leave)
label1.grid(row=0,column=0,columnspan=5)
bottondir.grid(row=1,column=0,columnspan=5,pady=20)
labelpass.grid(row=3,column=0,pady=20)
pass_entry.grid(row=3,column=1,columnspan=3)
label2.grid(row=2,column=0,pady=20)
fechaentry1.grid(row=2,column=1, pady=20)
label3.grid(row=2,column=2,pady=20)
fechaentry2.grid(row=2,column=3)
endbutton.grid(row=7,column=0,padx=10,columnspan=5)
progress.grid(row=6,column=0,pady=20,columnspan=5)
root.mainloop()